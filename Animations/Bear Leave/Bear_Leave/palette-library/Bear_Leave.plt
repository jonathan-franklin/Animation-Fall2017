ToonBoomAnimationInc PaletteFile 2
Solid    Black                      0x08d15dabba500b18   0   0   0 255
Solid    White                      0x08d15dabba500b1a 255 255 255 255
Solid    Red                        0x08d15dabba500b1c 255   0   0 255
Solid    Green                      0x08d15dabba500b1e   0 255   0 255
Solid    Blue                       0x08d15dabba500b20   0   0 255 255
Solid    "Vectorized Line"          0x0000000000000003   0   0   0 255
Solid    "New 1"                    0x08d55184bda04ab5   0   0   0   0
Solid    Eyes                       0x08deaa9566ed1a88  41 216   0 255
Solid    Sillouhette                0x08deaa9566ed4f95   0   0   0 255
Solid    Claws/Teeth                0x08deaa9566ed8085 245 231 189 255
Solid    Fur                        0x08ded9a1d433fb92   0   0   0 255
Solid    "New Highlight"            0x08dec69b7cc042d7  72  95 103 255
